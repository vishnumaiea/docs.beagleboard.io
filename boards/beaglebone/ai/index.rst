.. _beaglebone-ai-home:

BeagleBone AI
###############

BeagleBone AI is based on the Texas Instruments AM5729 dual-core Cortex-A15 SoC with flexible BeagleBone Black header and mechanical compatibility. BeagleBone AI makes it easy to explore how artificial intelligence (AI) can be used in everyday life via the TI C66x digital-signal-processor (DSP) cores and embedded-vision-engine (EVE) cores supported through an optimized TIDL machine learning OpenCL API with pre-installed tools. Focused on everyday automation in industrial, commercial and home applications.

.. admonition:: License Terms

    .. figure:: images/OSHW_mark_US000169.*
        :width: 200
        :target: https://certification.oshwa.org/us000169.html
        :alt: BeagleBone AI OSHW Mark
        :align: left

    * This documentation is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__
    * Design materials and license can be found in the `git repository <https://git.beagleboard.org/beagleboard/beaglebone-ai>`__
    * Use of the boards or design materials constitutes an agreement to the :ref:`boards-terms-and-conditions`
    * Software images and purchase links available on the `board page <https://www.beagleboard.org/boards/beaglebone-ai>`__
    * For export, emissions and other compliance, see :ref:`beaglebone-ai-support`

.. image:: images/BB_AI_handheld_500px.jpg
   :width: 500
   :align: center
   :height: 333
   :alt: BeagleBone AI

.. toctree::
    :maxdepth: 1

    ch01.rst
    ch03.rst
    ch04.rst
    ch05.rst
    ch06.rst
    ch07.rst
    ch08.rst
    ch11.rst

