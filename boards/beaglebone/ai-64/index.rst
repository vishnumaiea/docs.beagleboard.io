.. _bbai64-home:

BeagleBone AI-64
###################

BeagleBone® AI-64 brings a complete system for developing artificial intelligence (AI) and machine learning 
solutions with the convenience and expandability of the BeagleBone® platform and the peripherals on board to 
get started right away learning and building applications. With locally hosted, ready-to-use, open-source 
focused tool chains and development environment, a simple web browser, power source and network connection 
are all that need to be added to start building performance-optimized embedded applications. Industry-leading 
expansion possibilities are enabled through familiar BeagleBone® cape headers, with hundreds of open-source 
hardware examples and dozens of readily available embedded expansion options available off-the-shelf.

.. admonition:: OSHWA Certification mark

    .. figure:: images/OSHW_mark_US002120.*
        :width: 200
        :target: https://certification.oshwa.org/us002120.html
        :alt: BeagleBone AI-64 OSHW Mark

.. note::

    This work is licensed under a `Creative Commons Attribution-ShareAlike
    4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__

    Hardware design files can be found at https://git.beagleboard.org/beagleboard/beaglebone-ai-64

.. tip::
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page. 
    
    Use of either the boards or the design materials constitutes agreement to the T&C including any 
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.

.. toctree::
   :maxdepth: 1

   /boards/beaglebone/ai-64/ch01.rst
   /boards/beaglebone/ai-64/ch02.rst
   /boards/beaglebone/ai-64/ch03.rst
   /boards/beaglebone/ai-64/ch04.rst
   /boards/beaglebone/ai-64/ch05.rst
   /boards/beaglebone/ai-64/ch07.rst
   /boards/beaglebone/ai-64/ch09.rst
   /boards/beaglebone/ai-64/ch10.rst
   /boards/beaglebone/ai-64/ch11.rst
   /boards/beaglebone/ai-64/update.rst
   /boards/beaglebone/ai-64/edge_ai_apps/index.rst
