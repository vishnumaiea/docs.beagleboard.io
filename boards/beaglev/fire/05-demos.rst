.. _beaglev-fire-demos:

Demos
#####

.. todo::

    We need a CSI capture demos

.. todo::

    We need a cape compatibility layer demo

.. toctree:: 
    :maxdepth: 1
    
    demos-and-tutorials/flashing-board
    demos-and-tutorials/mchp-fpga-tools-installation-guide