.. _beaglev-fire-home:

BeagleV-Fire
###################

.. image:: media/BeagleV-Fire-hero.*
    :align: center
    :alt: BeagleV-Fire hero image

.. important::
    This is a work in progress, for latest documentation please 
    visit https://docs.beagleboard.org/latest/

.. admonition:: Contributors

    This work is licensed under a `Creative Commons Attribution-ShareAlike
    4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__

.. note::
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page.

    Use of either the boards or the design materials constitutes agreement to the T&C including any
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.

.. only:: html

  .. grid:: 1 1 2 3
    :margin: 4 4 0 0
    :gutter: 4

    .. grid-item-card::
        :link: beaglev-fire-introduction
        :link-type: ref

        **1. Introduction**
        ^^^

        .. image:: media/chapter-thumbnails/01-introduction.*
            :align: center
            :alt: BeagleV-Fire Chapter01 thumbnail
        
        +++

        Introduction to BeagleV-Fire board with information on each component 
        location on both front and back of the board.

    .. grid-item-card:: 
        :link: beaglev-fire-quick-start
        :link-type: ref

        **2. Quick start**
        ^^^

        .. image:: media/chapter-thumbnails/02-quick-start.*
            :align: center
            :alt: BeagleV-Fire Chapter02 thumbnail

        +++

        Getting started guide to enable you to start building your projects 
        in no time.

    .. grid-item-card:: 
        :link: beaglev-fire-design
        :link-type: ref

        **3. Design & Specifications**
        ^^^

        .. image:: media/chapter-thumbnails/03-design-and-specifications.*
            :align: center
            :alt: BeagleV-Fire Chapter03 thumbnail

        +++

        Hardware and mechanical design and specifications of BeagleV-Fire board 
        for those who want to know their board inside and out.

    .. grid-item-card:: 
        :link: beaglev-fire-expansion
        :link-type: ref

        **4. Expansion**
        ^^^

        .. image:: media/chapter-thumbnails/04-connectors-and-pinouts.*
            :align: center
            :alt: BeagleV-Fire Chapter04 thumbnail

        +++

        Connector pinout diagrams with expansion details so that you can 
        easily debug your connections and create custom expansion hardware.
        
    .. grid-item-card:: 
        :link: beaglev-fire-demos
        :link-type: ref

        **5. Demos**
        ^^^

        .. image:: media/chapter-thumbnails/05-demos-and-tutorials.*
            :align: center
            :alt: BeagleV-Fire Chapter5 thumbnail

        +++

        Demos and tutorials to quickly learn about BeagleV-Fire capabilities.

    .. grid-item-card:: 
        :link: beaglev-fire-support
        :link-type: ref

        **6. Support**
        ^^^

        .. image:: media/chapter-thumbnails/06-support-documents.*
            :align: center
            :alt: BeagleV-Fire Chapter6 thumbnail

        +++

        Additional supporting information, images, documents, change history and
        hardware & software repositories including issue trackers.


.. toctree::
   :maxdepth: 1
   :hidden:

   01-introduction
   02-quick-start
   03-design
   04-expansion
   05-demos
   06-support




